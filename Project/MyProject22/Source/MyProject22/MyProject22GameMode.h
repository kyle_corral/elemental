// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject22GameMode.generated.h"

UCLASS(minimalapi)
class AMyProject22GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyProject22GameMode();
};



