// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MyProject22GameMode.h"
#include "MyProject22PlayerController.h"
#include "MyProject22Character.h"
#include "UObject/ConstructorHelpers.h"

AMyProject22GameMode::AMyProject22GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyProject22PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}