// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT22_MyProject22Character_generated_h
#error "MyProject22Character.generated.h already included, missing '#pragma once' in MyProject22Character.h"
#endif
#define MYPROJECT22_MyProject22Character_generated_h

#define MyProject22_Source_MyProject22_MyProject22Character_h_12_RPC_WRAPPERS
#define MyProject22_Source_MyProject22_MyProject22Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject22_Source_MyProject22_MyProject22Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyProject22Character(); \
	friend MYPROJECT22_API class UClass* Z_Construct_UClass_AMyProject22Character(); \
public: \
	DECLARE_CLASS(AMyProject22Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MyProject22"), NO_API) \
	DECLARE_SERIALIZER(AMyProject22Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject22_Source_MyProject22_MyProject22Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyProject22Character(); \
	friend MYPROJECT22_API class UClass* Z_Construct_UClass_AMyProject22Character(); \
public: \
	DECLARE_CLASS(AMyProject22Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MyProject22"), NO_API) \
	DECLARE_SERIALIZER(AMyProject22Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject22_Source_MyProject22_MyProject22Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyProject22Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProject22Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject22Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject22Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject22Character(AMyProject22Character&&); \
	NO_API AMyProject22Character(const AMyProject22Character&); \
public:


#define MyProject22_Source_MyProject22_MyProject22Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject22Character(AMyProject22Character&&); \
	NO_API AMyProject22Character(const AMyProject22Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject22Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject22Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyProject22Character)


#define MyProject22_Source_MyProject22_MyProject22Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(AMyProject22Character, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMyProject22Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(AMyProject22Character, CursorToWorld); }


#define MyProject22_Source_MyProject22_MyProject22Character_h_9_PROLOG
#define MyProject22_Source_MyProject22_MyProject22Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_PRIVATE_PROPERTY_OFFSET \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_RPC_WRAPPERS \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_INCLASS \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject22_Source_MyProject22_MyProject22Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_PRIVATE_PROPERTY_OFFSET \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_INCLASS_NO_PURE_DECLS \
	MyProject22_Source_MyProject22_MyProject22Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject22_Source_MyProject22_MyProject22Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
