// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT22_MyProject22PlayerController_generated_h
#error "MyProject22PlayerController.generated.h already included, missing '#pragma once' in MyProject22PlayerController.h"
#endif
#define MYPROJECT22_MyProject22PlayerController_generated_h

#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_RPC_WRAPPERS
#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyProject22PlayerController(); \
	friend MYPROJECT22_API class UClass* Z_Construct_UClass_AMyProject22PlayerController(); \
public: \
	DECLARE_CLASS(AMyProject22PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/MyProject22"), NO_API) \
	DECLARE_SERIALIZER(AMyProject22PlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyProject22PlayerController(); \
	friend MYPROJECT22_API class UClass* Z_Construct_UClass_AMyProject22PlayerController(); \
public: \
	DECLARE_CLASS(AMyProject22PlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/MyProject22"), NO_API) \
	DECLARE_SERIALIZER(AMyProject22PlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyProject22PlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProject22PlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject22PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject22PlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject22PlayerController(AMyProject22PlayerController&&); \
	NO_API AMyProject22PlayerController(const AMyProject22PlayerController&); \
public:


#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject22PlayerController(AMyProject22PlayerController&&); \
	NO_API AMyProject22PlayerController(const AMyProject22PlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject22PlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject22PlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyProject22PlayerController)


#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_9_PROLOG
#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_RPC_WRAPPERS \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_INCLASS \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_INCLASS_NO_PURE_DECLS \
	MyProject22_Source_MyProject22_MyProject22PlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject22_Source_MyProject22_MyProject22PlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
